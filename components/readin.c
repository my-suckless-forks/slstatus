/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <stdlib.h>
#include "../util.h"

const char *
readin( )
{
  size_t size=30;
  int bytesRead;

  char *string;
  /* These 2 lines are very important. */
  string = (char *)malloc(size);
  bytesRead = getline(&string, &size, stdin);

  if (bytesRead == -1)
  {
    return "|";
  }else
  {
  return string;
  }
return"|";

}
